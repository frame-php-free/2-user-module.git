<?php

namespace UserModule\service;

/**
 * 用户其他操作
 * Class UserOpt
 * @package UserModule\service
 */
class UserOpt
{
    /**
     * 注册操作
     */
    public function register()
    {

    }

    /**
     * 修改密码
     */
    public function updatePassword()
    {

    }
}