<?php

namespace UserModule;

/**
 * diy 模块路由配置
 * Class Router
 * @package DiyModule
 */
class Router
{
    /**
     * 用户模块路由
     * @param $uri
     * @return bool
     */
    public function run($uri)
    {
        session_start();
        define("USER_MODULE_TEMPLATE", __DIR__);
        switch ($uri) {
            case "/user-module/login/index":
                (new c\web\Login())->index();
                break;
            case "/user-module/login/out":
                (new c\api\Login())->out();
                break;
            case "/user-module/login/phone":
                (new c\api\Login())->phoneLogin();
                break;
            case "/user-module/table/init":
                (new c\api\Init())->initTable();
                break;
            default:
                return false;
        }
        return true;
    }
}