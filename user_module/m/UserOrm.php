<?php

namespace UserModule\m;

/**
 * 用户orm
 * Class UserOrm
 * @package UserModule\m
 */
class UserOrm
{
    const USER_TABLE_NAME = "user_main";

    public $uid;
    public $rid;
    public $home_url;
    public $user_name;
    public $password;
    public $salt;
    public $name;
    public $phone;
    public $avatar;

    /**
     * 属性映射
     * @param $arrayData
     */
    public function initData($arrayData)
    {
        foreach ($this as $k => $v) {
            $this->{$k} = $arrayData[$k];
        }
    }
}