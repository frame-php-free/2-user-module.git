<?php

namespace UserModule\c\api;

use UserModule\service\UserLogin;

class Login
{
    /**
     * 构造方法
     * Index constructor.
     */
    public function __construct()
    {
        header('Content-type: application/json');
    }

    /**
     * http://www.free-php-2.cn/user-module/login/phone?phone=182&password=admin
     */
    public function phoneLogin()
    {
        if (UserLogin::checkLogin()) {
            echo json_encode(['code' => 200, 'message' => "is login"]);
            return;
        }
        $phone = $_POST['phone'];
        $password = $_POST['password'];
        $server = new UserLogin();
        $result = $server->loginByPhonePassword($phone, $password);
        if ($result) {
            echo json_encode(['code' => 200, 'message' => "success"]);
            return;
        }
        echo json_encode(['code' => 500, 'message' => "error"]);
        return;
    }

    /**
     * http://www.free-php-2.cn/user-module/login/out
     */
    public function out()
    {
        UserLogin::logout();
        echo json_encode(['code' => 200, 'message' => "success"]);
    }
}