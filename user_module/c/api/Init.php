<?php

namespace UserModule\c\api;

use UserModule\service\ModelTableInit;

/**
 * 初始化表操作
 * Class Init
 * @package UserModule\c\api
 */
class Init
{
    /**
     * 构造方法
     * Index constructor.
     */
    public function __construct()
    {
        header('Content-type: application/json');
    }

    /**
     * 初始化表
     * http://www.free-php-2.cn/user-module/table/init
     */
    public function initTable()
    {
        $server = new ModelTableInit();
        $server->initRole();
        $server->initUser();
        echo json_encode(['code' => 200, 'msg' => "over pleas check"]);
    }
}