<?php

namespace UserModule\c\web;


/**
 * demo web
 * Class Home
 * @package DiyModule\c\web
 */
class Login
{
    /**
     * 构造方法
     * Index constructor.
     */
    public function __construct()
    {
        header('Content-Type: text/html; charset=utf-8');
    }

    /**
     * 示例模版页面
     * http://www.free-php.cn/user-module/login/index
     */
    public function index()
    {
        $my_array = array(
            "title" => "template demo",
            "content" => "content",
        );
        extract($my_array);
        include(USER_MODULE_TEMPLATE . "/templates/user/login.php");
    }

}