# 2-user-module

#### 项目介绍
- frame-php-free 系列项目的子项目用于被拼装

#### 软件架构

- 依赖composer进行类加载
- public/index.php 是入口文件
- use_module 用户模块

#### 安装教程

1. 下载本项目（作为主项目时的配置）
2. 执行composer install
3. 启动服务(nginx服务 或者 php自带服务)
##### nginx 配置(free-php是生产的项目名)
```
server {
        listen 80;
        server_name www.free-php.cn;

        root /d-code/frame-php-free/2-user-module/public;
        index index.php index.html;

        if (!-e $request_filename) {
                rewrite ^/(.*) /index.php/$1 last;
        }

        location ~* \.php {
                fastcgi_pass  php-fpm.docker:9000;
                fastcgi_index index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root/$fastcgi_script_name;
                include fastcgi_params;
        }

        location ~ /\.git {
                deny all;
        }
}
```
- www.free-php.cn
- www.free-php.cn/home/index?a=1

##### php 内置服务
```
cd 2-user-module
composer update
cd public
php -S localhost:8001
```

- localhost:8001
- localhost:8001/home/index?a=1

#### 使用说明

- 主项目的composer.json 中添加该模块的依赖
- 执行composer install （autoload-dev 失效防止和主项目config冲突）
- 主项目public/index.php 注册该模块路由
- public下到静态文件要同步到主项目根目录下到public目录
- 需要修改源码（定制开发），可将该模块目录复制到主项目根目录直接修改，删掉composer此模块依赖


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 常用的git操作
```
git tag -a 1.0.0 -m 'login page'
git push origin 1.0.0

```