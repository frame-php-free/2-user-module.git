<?php
// free-php 入口文件
define("USER_MODULE_VIEW_PATH", __DIR__ . "/");

// composer 自动加载脚本
require_once __DIR__ . "/../vendor/autoload.php";

// 路由配置
$uri = $_SERVER['REQUEST_URI'];
$uri = strpos($uri, "?") !== false ? reset(explode("?", $uri)) : $uri;
// 各个模块的路由注册
if ((new UserModule\Router())->run($uri)) return;

http_response_code(404);
return;